'use strict';

const del = require('del');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

const bundle = [
    // TODO Remove useless files (from bootstrap…).
    {
        'source': 'node_modules/animate.css/animate.compat.css',
        'dest': 'asset/vendor/animate.css',
    },
    {
        'source': 'node_modules/bootstrap/dist/**',
        'dest': 'asset/vendor/bootstrap',
    },
    // TODO Minify css and js.
    /*
    {
        'source': 'node_modules/daterangepicker/daterangepicker.css',
        'dest': 'asset/vendor/daterangepicker',
    },
    {
        'source': 'node_modules/daterangepicker/daterangepicker.js',
        'dest': 'asset/vendor/daterangepicker',
    },
    */
    {
        'source': 'node_modules/ionicons/dist/css/**',
        'dest': 'asset/vendor/ionicons/css',
    },
    {
        'source': 'node_modules/ionicons/dist/fonts/**',
        'dest': 'asset/vendor/ionicons/fonts',
    },
    {
        'source': 'node_modules/jquery/dist/jquery.min.js',
        'dest': 'asset/vendor/jquery',
    },
    {
        'source': 'node_modules/jquery-migrate/dist/jquery-migrate.min.js',
        'dest': 'asset/vendor/jquery',
    },
    {
        'source': 'node_modules/moment/min/moment.min.js',
        'dest': 'asset/vendor/moment',
    },
    /*
    {
        'source': 'node_modules/owl-carousel/**',
        'dest': 'asset/vendor/owl-carousel',
    },
    */
    {
        'source': 'node_modules/pretty-checkbox/dist/pretty-checkbox.min.css',
        'dest': 'asset/vendor/pretty-checkbox',
    },
    {
        'source': 'node_modules/waypoints/lib/jquery.waypoints.min.js',
        'dest': 'asset/vendor/waypoints',
    },
    // TODO Minify css and js.
    /*
    {
        'source': 'node_modules/wow/**',
        'dest': 'asset/vendor/wow',
    },
    */
];

gulp.task('clean', function(done) {
    bundle.forEach(function (module) {
        return del.sync(module.dest);
    });
    done();
});

gulp.task('sync', function (done) {
    bundle.forEach(function (module) {
        gulp.src(module.source)
            .pipe(gulpif(module.rename && module.rename.length, rename(module.rename)))
            .pipe(gulpif(module.suffix, rename({suffix: module.suffix})))
            .pipe(gulpif(module.uglify, uglify()))
            .pipe(gulp.dest(module.dest));
    });
    done();
});

gulp.task('default', gulp.series('clean', 'sync'));

gulp.task('install', gulp.task('default'));

gulp.task('css', function () {
    var sass = require('gulp-sass');
    var postcss = require('gulp-postcss');
    var autoprefixer = require('autoprefixer');

    return gulp.src('./asset/sass/*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: ['node_modules/susy/sass']
        }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(gulp.dest('./asset/css'));
});

gulp.task('css:watch', function () {
    gulp.watch('./asset/sass/*.scss', gulp.parallel('css'));
});
