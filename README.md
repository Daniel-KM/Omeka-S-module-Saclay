Saclay (module for Omeka S)
==========================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the previous repository.__

[Saclay] est un module pour [Omeka S] qui contient toutes les fonctionnalités
spécifiques au site [Saclay communication]. Il fonctionne avec le [thème Saclay]


Installation
------------

The module requires the modules [Advanced Search Plus], [Block Plus] and
optionnaly [Generic]. Optional modules are [ApiInfo], [Reference], and [Next].

See general end user documentation for [installing a module].

* From the zip

Download the last release [Saclay.zip] from the list of releases, and uncompress
it in the `modules` directory.

* From the source and for development

If the module was installed from the source, rename the name of the folder of
the module to `Saclay`, go to the root of the module, and run:

```sh
npm install
gulp
```


Usage
-----

### Modification de la page de redirection après login

La page après login peut être modifiée dans la configuration du module.

### Affichage des modèles de ressource dans la recherche avancée des items.

Un champ de recherche complémentaire liste les modèles de ressources dans la
partie publique.

### Bloc de la page d'accueil

Le bloc de la page d'accueil permet de faire des recherches et de les filtrer
sans recharger la page.


TODO
----

- [ ] La requête dans le bloc ne peut pas utiliser de collections, car l'api permet seulement d'utiliser "ou" pour les collections.
- [ ] De même pour datetime.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [module issues] page on GitLab.


License
-------

This module is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

This software is governed by the CeCILL license under French law and abiding by
the rules of distribution of free software. You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated by
CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors have only limited liability.

In this respect, the user’s attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by the
user in light of its specific status of free software, that may mean that it is
complicated to manipulate, and that also therefore means that it is reserved for
developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software’s suitability as
regards their requirements in conditions enabling the security of their systems
and/or data to be ensured and, more generally, to use and operate it in the same
conditions as regards security.

The fact that you are presently reading this means that you have had knowledge
of the CeCILL license and that you accept its terms.


Copyright
---------

* Copyright Daniel Berthereau, 2019-2021 (integration for [Sempiternelia], see [Daniel-KM] on GitLab)


[Saclay]: https://gitlab.com/Daniel-KM/Omeka-S-module-Saclay
[Omeka S]: https://omeka.org/s
[Saclay communication]: https://www.universite-paris-saclay.fr/
[thème Saclay]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Saclay
[installing a module]: http://dev.omeka.org/docs/s/user-manual/modules/#installing-modules
[Advanced Search Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedSearchPlus
[Block Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-BlockPlus
[Generic]: https://gitlab.com/Daniel-KM/Omeka-S-module-Generic
[ApiInfo]: https://gitlab.com/Daniel-KM/Omeka-S-module-ApiInfo
[Reference]: https://gitlab.com/Daniel-KM/Omeka-S-module-Reference
[Next]: https://gitlab.com/Daniel-KM/Omeka-S-module-Next
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-Saclay/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[GitLab]: https://gitlab.com/Daniel-KM
[Sempiternelia]: https://sempiternelia.net
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
