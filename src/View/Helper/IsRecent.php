<?php declare(strict_types=1);

namespace Saclay\View\Helper;

use Omeka\Api\Representation\ItemRepresentation;
use Laminas\View\Helper\AbstractHelper;

class IsRecent extends AbstractHelper
{
    /**
     * Check if the specified item is a recent one, according to the specified date.
     *
     * Note: results are statically saved, so "recentDays" and "recentLast"
     * should be set the first time only. Or use "reset".
     */
    public function __invoke(ItemRepresentation $item, ?int $recentDays = null, ?int $recentLast = null, bool $reset = false): bool
    {
        static $recentDaysStatic;
        static $recents;
        static $now;

        if (is_null($recents) || $reset) {
            $recentDaysStatic = $recentDays;
            if ($recentLast) {
                // View api doesn't allow to return scalar.
                $api = $item->getServiceLocator()->get('Omeka\ApiManager');
                $site = $this->currentSite();
                $recents = $api
                    ->search('items', ['site_id', $site->id(), 'sort_by' => 'created', 'sort_order' => 'DESC', 'limit' => $recentLast], ['returnScalar' => 'id'])
                    ->getContent();
                $recents = array_fill_keys($recents, true);
            } else {
                $recents = [];
            }

            $now = new \DateTime();
        }

        return isset($recents[$item->id()])
            || ($recentDaysStatic && $item->created()->diff($now)->days < $recentDaysStatic);
    }

    /**
     * Get the current site.
     */
    protected function currentSite(): ?\Omeka\Api\Representation\SiteRepresentation
    {
        return $this->view->site ?? $this->view
            ->getHelperPluginManager()
            ->get('Laminas\View\Helper\ViewModel')
            ->getRoot()
            ->getVariable('site');
    }
}
