<?php declare(strict_types=1);

namespace Saclay\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Omeka\Api\Representation\SiteRepresentation;

class ListSiteItemSets extends AbstractHelper
{
    /**
     * Get the list of ids and titles of the site item sets.
     */
    public function __invoke(SiteRepresentation $site = null): array
    {
        if (!$site) {
            $site = $this->currentSite();
            if (!$site) {
                return [];
            }
        }

        $api = $this->getView()->plugin('api');

        $result = [];
        foreach ($site->siteItemSets() as $siteItemSet) {
            try {
                $itemSet = $api->read('item_sets', $siteItemSet->itemSet()->id())->getContent();
                $result[$itemSet->id()] = $itemSet->displayTitle();
            } catch (\Omeka\Api\Exception\ExceptionInterface $e) {
            }
        }

        return $result;
    }

    /**
     * Get the current site.
     */
    protected function currentSite(): ?SiteRepresentation
    {
        return $this->view->site ?? $this->view
            ->getHelperPluginManager()
            ->get('Laminas\View\Helper\ViewModel')
            ->getRoot()
            ->getVariable('site');
    }
}
