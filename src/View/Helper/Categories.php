<?php declare(strict_types=1);

namespace Saclay\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class Categories extends AbstractHelper
{
    protected $categoryType = 'disable';
    protected $options = [
        'resource_classes' => [],
        'resource_templates' => [],
    ];

    public function __invoke(): self
    {
        return $this;
    }

    public function setOptions(string $categoryType, array $options = []): self
    {
        if (in_array($categoryType, [
            'resource_classes',
            'resource_templates',
            'resource_classes_selected',
            'resource_templates_selected',
        ])) {
            $this->categoryType = $categoryType;
        } else {
            $this->categoryType = 'disable';
        }
        $this->options = $options + [
            'resource_classes' => [],
            'resource_templates' => [],
        ];
        return $this;
    }

    /**
     * Get the list of categories defined in the theme settings.
     *
     * @param string|array $sort May be "alphabetic", "total", or an array.
     * @param array $icons
     * @return []
     */
    public function list($sort = null, array $icons = []): array
    {
        if ($this->categoryType === 'disable') {
            return [];
        }

        $view = $this->getView();
        $api = $view->api();

        $categories = [];

        // TODO Limit list to the current site.
        $resourceName = $this->categoryType;
        if ($resourceName === 'resource_classes_selected') {
            $list = $this->options['resource_classes'];
            $reprs = [];
            foreach ($list as $term) {
                if ($value = $api->searchOne('resource_classes', ['term' => $term])->getContent()) {
                    $reprs[] = $value;
                }
            }
            $resourceName = 'resource_classes';
        } elseif ($resourceName === 'resource_templates_selected') {
            $list = $this->options['resource_templates'];
            $reprs = [];
            foreach ($list as $id) {
                try {
                    $value = $api->read('resource_templates', ['id' => $id])->getContent();
                    $reprs[] = $value;
                } catch (\Exception $e) {
                    $value = $api->searchOne('resource_templates', ['label' => $id])->getContent();
                    if ($value) {
                        $reprs[] = $value;
                    }
                }
            }
            $resourceName = 'resource_templates';
        } else {
            // Use module Next to filter only used resource classes. Anyway, it
            // is filtered below.
            $reprs = $api->search($resourceName, ['used' => true])->getContent();
        }

        foreach ($reprs as $repr) {
            $total = $repr->itemCount();
            if ($total) {
                $categories[$repr->id()] = [
                    'id' => $repr->id(),
                    'label' => $repr->label(),
                    'total' => $total,
                ];
            }
        }

        // mbstrings is provided by omeka/composer if missing.

        if ($sort === 'alphabetic') {
            uasort($categories, function ($a, $b) {
                return strnatcasecmp($a['label'], $b['label']);
            });
        } elseif ($sort === 'total') {
            uasort($categories, function ($a, $b) {
                if ($a['total'] === $b['total']) {
                    return 0;
                }
                return ($a['total'] > $b['total']) ? -1 : 1;
            });
        } elseif (is_array($sort)) {
            $list = array_replace(array_map(function ($v) {
                return mb_strtolower($v['label']);
            }, $categories));
            $list = array_flip(array_replace(array_flip(array_intersect($sort, $list)), array_flip($list)));
            $categories = array_replace($list, $categories);
        }

        $theme = $this->currentSite()->theme();
        $source = $theme && file_exists(OMEKA_PATH . '/themes/' . $theme . '/data/category_icons.php')
            ? OMEKA_PATH . '/themes/' . $theme . '/data/category_icons.php'
            : dirname(__DIR__, 3) . '/data/category_icons.php';
        $loadedIcons = include $source;
        $direct = is_array($loadedIcons);

        if ($direct) {
            $icons = array_filter($loadedIcons);
            $countIcons = count($loadedIcons);
            if (!$countIcons) {
                return $categories;
            }
        }

        $icons = array_change_key_case($icons);

        if ($direct) {
            $key = 0;
            foreach ($categories as &$category) {
                $label = mb_strtolower($category['label']);
                $category['icon'] = empty($icons[$label])
                    ? 'no_image'
                    : $icons[$label];
            }
            unset($category);

            return $categories;
        }

        $key = 0;
        foreach ($categories as &$category) {
            $category['icon'] = in_array($label = mb_strtolower($category['label']), $icons)
                ? $label
                : $icons[++$key % $countIcons];
        }
        unset($category);

        return $categories;
    }

    /**
     * Get the current site.
     */
    protected function currentSite(): ?\Omeka\Api\Representation\SiteRepresentation
    {
        return $this->view->site ?? $this->view
            ->getHelperPluginManager()
            ->get('Laminas\View\Helper\ViewModel')
            ->getRoot()
            ->getVariable('site');
    }
}
