<?php declare(strict_types=1);

namespace Saclay\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Omeka\Api\Representation\ItemRepresentation;

class IsFeatured extends AbstractHelper
{
    /**
     * Check if the specified item is a featured one (in the specified item set).
     */
    public function __invoke(ItemRepresentation $item, ?int $featuredItemSetId): bool
    {
        if (empty($featuredItemSetId)) {
            return false;
        }

        // Item sets are an associative array.
        $itemSets = $item->itemSets();
        return isset($itemSets[$featuredItemSetId]);
    }
}
