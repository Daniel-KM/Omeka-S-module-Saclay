<?php declare(strict_types=1);

namespace Saclay\Site\BlockLayout;

use Laminas\View\Renderer\PhpRenderer;
use Omeka\Api\Representation\SitePageBlockRepresentation;
use Omeka\Api\Representation\SitePageRepresentation;
use Omeka\Api\Representation\SiteRepresentation;

/**
 * @todo Improve Block Plus Search results to make it extendable simpler.
 */
class SearchResultsFilters extends \BlockPlus\Site\BlockLayout\SearchResults
{
    /**
     * The default partial view script.
     */
    const PARTIAL_NAME = 'common/block-layout/search-results-filters';

    public function getLabel()
    {
        return 'Search form and results with filters'; // @translate
    }

    public function prepareRender(PhpRenderer $view)
    {
        $assetUrl = $view->plugin('assetUrl');
        $view->headlink()
            // ->appendStylesheet('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')
            ->appendStylesheet($assetUrl('vendor/bootstrap/css/bootstrap.min.css'))
            ->appendStylesheet($assetUrl('vendor/animate.css/animate.compat.css', 'Saclay'))
            ->appendStylesheet($assetUrl('vendor/ionicons/css/ionicons.min.css', 'Saclay'))
            ->appendStylesheet($assetUrl('vendor/owlcarousel/assets/owl.carousel.min.css', 'Saclay'))
            ->appendStylesheet($assetUrl('vendor/lightbox/css/lightbox.min.css', 'Saclay'))
            ->appendStylesheet($assetUrl('vendor/daterangepicker/daterangepicker.min.css', 'Saclay'))
            ->appendStylesheet($assetUrl('vendor/pretty-checkbox/pretty-checkbox.min.css', 'Saclay'))
        ;
        $view->headScript()
            // Attention : jquery-migrate, bootstrap.bundle et easing peuvent empêcher d'autres bibliothèques (UniversalViewer ou Mirador).
            ->appendFile($assetUrl('vendor/jquery/jquery-migrate.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/bootstrap/js/bootstrap.bundle.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/easing/easing.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/wow/wow.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/waypoints/jquery.waypoints.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/owlcarousel/owl.carousel.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/counterup/jquery.counterup.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/isotope/isotope.pkgd.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/lightbox/js/lightbox.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/moment/moment.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/daterangepicker/daterangepicker.min.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('vendor/contactform/contactform.js', 'Saclay'), 'text/javascript', ['defer' => 'defer'])
            // Custom js.
            ->appendFile($assetUrl('js/search-results-filters-main.js', 'Saclay', true), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('js/search-results-filters.js', 'Saclay',  true), 'text/javascript', ['defer' => 'defer'])
        ;
    }

    public function form(
        PhpRenderer $view,
        SiteRepresentation $site,
        SitePageRepresentation $page = null,
        SitePageBlockRepresentation $block = null
    ) {
        // Factory is not used to make rendering simpler.
        $services = $site->getServiceLocator();
        $formElementManager = $services->get('FormElementManager');
        $defaultSettings = $services->get('Config')['saclay']['block_settings']['searchResultsFilters'];
        $blockFieldset = \Saclay\Form\SearchResultsFiltersFieldset::class;

        $data = $block ? $block->data() + $defaultSettings : $defaultSettings;

        $data['query'] = http_build_query($data['query']);

        $dataForm = [];
        foreach ($data as $key => $value) {
            $dataForm['o:block[__blockIndex__][o:data][' . $key . ']'] = $value;
        }

        $fieldset = $formElementManager->get($blockFieldset);
        $fieldset->populateValues($dataForm);

        return $view->formCollection($fieldset);
    }

    public function render(PhpRenderer $view, SitePageBlockRepresentation $block)
    {
        // Similar to BrowsePreview::render(), but with a different query.
        // The filter is added to the first result for simplicity.

        $resourceType = $block->dataValue('resource_type', 'items');

        $defaultQuery = $block->dataValue('query', []) + ['search' => ''];
        $query = $view->params()->fromQuery() + $defaultQuery;

        $site = $block->page()->site();
        if ($view->siteSetting('browse_attached_items', false)) {
            $query['site_attachments_only'] = true;
        }

        // Allow to force to display resources from another site.
        if (empty($query['site_id'])) {
            $query['site_id'] = $site->id();
        }

        $limit = $block->dataValue('limit', 12) ?: 12;

        // Unlike browse preview, the pagination is always prepared, even if it
        // not displayed in the view.
        $showPagination = $limit && $block->dataValue('pagination');

        $currentPage = $view->params()->fromQuery('page', 1);
        $query['page'] = $currentPage;
        $query['per_page'] = $limit;

        $sortBy = $view->params()->fromQuery('sort_by');
        if ($sortBy) {
            $query['sort_by'] = $sortBy;
        } elseif (!isset($query['sort_by'])) {
            $query['sort_by'] = 'created';
        }

        $sortOrder = $view->params()->fromQuery('sort_order');
        if ($sortOrder) {
            $query['sort_order'] = $sortOrder;
        } elseif (!isset($query['sort_order'])) {
            $query['sort_order'] = 'desc';
        }

        $filters = $block->dataValue('filters') ?: ['featured', 'recent', 'all'];
        if (!is_array($filters)) {
            $filters = array_filter(array_map('trim', explode("\n", $filters)));
        }
        $featuredItemSet = $block->dataValue('featured_item_set') ?: null;
        $recentDays = $block->dataValue('recent_days') ?: 60;
        $recentLast = $block->dataValue('recent_last') ?: 36;
        $filterNewSearch = (bool) $block->dataValue('filter_new_search');

        $firstQuery = $query;
        $activeFilter = reset($filters);

        if ($filterNewSearch) {
            $checkFilterNewSearch = true;
        } else {
            $testQuery = $firstQuery;
            $testQuery = array_filter($testQuery);
            $checkFilterNewSearch = empty($testQuery['resource_class_id'])
                && empty($testQuery['resource_template_id'])
                && empty($testQuery['property'])
                && empty($testQuery['search'])
                && empty($testQuery['datetime'])
                && empty($testQuery['item_set_id']);
        }

        // FIXME Omeka api can only manage "or" item sets.
        if (isset($firstQuery['item_set_id']) && is_array($firstQuery['item_set_id'])) {
            $firstQuery['item_set_id'] = reset($firstQuery['item_set_id']);
        }

        if ($checkFilterNewSearch) {
            if ($activeFilter === 'featured') {
                if ($featuredItemSet) {
                    $firstQuery['item_set_id'] = $featuredItemSet;
                }
            } elseif ($activeFilter === 'recent') {
                if ($recentDays) {
                    $firstQuery['datetime'][] = [
                        'joiner' => 'and',
                        'field' => 'created',
                        'type' => 'gt',
                        // Don't use seconds to simplify storage.
                        'value' => date('Y-m-d', time() - $recentDays * 86400),
                    ];
                }
            }
        } else {
            $activeFilter = 'all';
        }

        /** @var \Omeka\Api\Response $response */
        $api = $view->api();
        $response = $api->search($resourceType, $firstQuery);

        // TODO Currently, there can be only one pagination by page.
        $totalCount = $response->getTotalResults();
        $view->pagination(null, $totalCount, $currentPage, $limit);

        /** @var \Omeka\Api\Representation\ResourceTemplateRepresentation $resourceTemplate */
        $resourceTemplate = $block->dataValue('resource_template');
        if ($resourceTemplate) {
            try {
                $resourceTemplate = $api->read('resource_templates', $resourceTemplate)->getContent();
            } catch (\Exception $e) {
            }
        }

        $sortHeadings = $block->dataValue('sort_headings', []);
        if ($sortHeadings) {
            $translate = $view->plugin('translate');
            foreach ($sortHeadings as $key => $sortHeading) {
                switch ($sortHeading) {
                    case 'created':
                        $label = $translate('Created'); // @translate
                        break;
                    case 'resource_class_label':
                        $label = $translate('Class'); // @translate
                        break;
                    default:
                        $property = $api->searchOne('properties', ['term' => $sortHeading])->getContent();
                        if ($property) {
                            if ($resourceTemplate) {
                                $templateProperty = $resourceTemplate->resourceTemplateProperty($property->id());
                                if ($templateProperty) {
                                    $label = $translate($templateProperty->alternateLabel() ?: $property->label());
                                    break;
                                }
                            }
                            $label = $translate($property->label());
                        } else {
                            unset($sortHeadings[$key]);
                            continue 2;
                        }
                        break;
                }
                $sortHeadings[$key] = [
                    'label' => $label,
                    'value' => $sortHeading,
                ];
            }
            $sortHeadings = array_filter($sortHeadings);
        }

        $resources = $response->getContent();

        $resourceTypes = [
            'items' => 'item',
            'item_sets' => 'item-set',
            'media' => 'media',
        ];

        $vars = [
            'site' => $view->site,
            'heading' => $block->dataValue('heading'),
            'resourceType' => $resourceTypes[$resourceType],
            'resources' => $resources,
            'query' => $firstQuery,
            'pagination' => $showPagination,
            'sortHeadings' => $sortHeadings,
            'filters' => $filters,
            'activeFilter' => $activeFilter,
            'filterNewSearch' => $filterNewSearch,
            'featuredItemSet' => $featuredItemSet,
            'recentDays' => $recentDays,
            'recentLast' => $recentLast,
            // Categories and filters.
            'categoryType' => $block->dataValue('category_type', 'resource_classes'),
            'categoryResourceClasses' => $block->dataValue('category_resource_classes', []),
            'categoryResourceTemplates' => $block->dataValue('category_resource_templates', []),
            'searchPropertyDateRange' => $block->dataValue('search_property_date_range', null),
            'searchPropertiesUnique' => $block->dataValue('search_properties_unique', null),
            'searchPropertiesCustomVocab' => $block->dataValue('search_properties_custom_vocab', ''),
            'searchInItemSets' => $block->dataValue('search_in_item_sets', false),
            'searchItemSets' => $block->dataValue('search_item_sets', []),
        ];
        $template = $block->dataValue('template', self::PARTIAL_NAME);
        return $view->resolver($template)
            ? $view->partial($template, $vars)
            : $view->partial(self::PARTIAL_NAME, $vars);
    }
}
