<?php declare(strict_types=1);
namespace Saclay\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class ConfigForm extends Form
{
    public function init(): void
    {
        $this
            ->add([
                'name' => 'saclay_redirect',
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Redirect page after login', // @translate
                    'info' => 'Set "home" for main home page (admin or public), "site" for the current site home, "me" for guest account, or any path starting with "/", including "/" itself for main home page.',
                ],
                'attributes' => [
                    'id' => 'saclay-redirect',
                    'required' => false,
                ],
            ])
        ;
    }
}
