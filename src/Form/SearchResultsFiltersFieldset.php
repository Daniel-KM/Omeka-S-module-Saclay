<?php declare(strict_types=1);

namespace Saclay\Form;

use BlockPlus\Form\SearchResultsFieldset;
use Laminas\Form\Element;
use Omeka\Form\Element\ArrayTextarea;
use Omeka\Form\Element\ItemSetSelect;
use Omeka\Form\Element\PropertySelect;
use Omeka\Form\Element\ResourceClassSelect;
use Omeka\Form\Element\ResourceTemplateSelect;

class SearchResultsFiltersFieldset extends SearchResultsFieldset
{
    public function init(): void
    {
        parent::init();

        $this
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][filters]',
                'type' => ArrayTextarea::class,
                'options' => [
                    'label' => 'Filtres du résultat', // @translate
                    'info' => 'Les trois champs sont : featured, recent, all (En vedette, Derniers ajouts, Tous)', // @translate
                ],
                'attributes' => [
                    'id' => 'search-results-filters-filters',
                    'rows' => 3,
                    'placeholder' => 'featured
recent
all',
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][filter_new_search]',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Utiliser les filtres pour les nouvelles recherches', // @translate
                    'info' => 'Si coché, le premier filtre (vedette, récent, tous) est pris en compte pour les nouvelles recherches.', // @translate
                ],
                'attributes' => [
                    'id' => 'search-results-filters-filter_new_search',
                ],
            ])

            ->add([
                'name' => 'o:block[__blockIndex__][o:data][featured_item_set]',
                'type' => ItemSetSelect::class,
                'options' => [
                    'label' => 'Featured item set', // @translate
                    'empty_option' => '',
                ],
                'attributes' => [
                    'id' => 'search-results-filters-featured_item_set',
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select an item set…', // @translate
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][recent_days]',
                'type' => Element\Number::class,
                'options' => [
                    'label' => 'Durée pour les documents récents', // @translate
                    'info' => 'Indiquer le nombre de jours au-delà duquel un document ne sera plus récent.', // @translate
                ],
                'attributes' => [
                    'id' => 'search-results-filters-recent_days',
                    'placeholder' => 60,
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][recent_last]',
                'type' => Element\Number::class,
                'options' => [
                    'label' => 'Nombre minimum de documents récents', // @translate
                    'info' => 'Si les documents récents ne sont pas assez nombreux, retournent également les derniers documents.', // @translate
                ],
                'attributes' => [
                    'id' => 'search-results-filters-recent_last',
                    'placeholder' => 36,
                ],
            ])

            ->add([
                'name' => 'o:block[__blockIndex__][o:data][category_type]',
                'type' => Element\Radio::class,
                'options' => [
                    'label' => 'Catégories (utilise les ico', // @translate
                    'info' => 'Les icones doivent être disponibles dans "asset/img" et correspondre aux noms.', // @translate
                    'value_options' => [
                        'resource_classes' => 'All resource classes', // @translate
                        'resource_templates' => 'All resource templates', // @translate
                        'resource_classes_selected' => 'Selected resource classes', // @translate
                        'resource_templates_selected' => 'Selected resource templates', // @translate
                        'disable' => 'Disable', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'search-results-filters-category_type',
                    'value' => 'resource_classes',
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][category_resource_classes]',
                'type' => ResourceClassSelect::class,
                'options' => [
                    'label' => 'Sélection de classes (si choisi ci-dessus)', // @translate
                    'empty_option' => '',
                    'term_as_value' => true,
                ],
                'attributes' => [
                    'id' => 'search-results-filters-category_resource_classes',
                    'multiple' => 'multiple',
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select resource classes…', // @translate
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][category_resource_templates]',
                'type' => ResourceTemplateSelect::class,
                'options' => [
                    'label' => 'Sélection de modèles (si choisi ci-dessus)', // @translate
                    'empty_option' => '',
                ],
                'attributes' => [
                    'id' => 'search-results-filters-category_resource_templates',
                    'multiple' => 'multiple',
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select resource templates…', // @translate
                ],
            ])

            /*
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][search_property_date_range]',
                'type' => PropertySelect::class,
                'options' => [
                    'label' => 'Propriété pour l’intervalle de date (TODO)', // @translate
                    'info' => 'Fonctionne seulement avec le module Advanced Search Plus (dates de création/modification) ou NumericDataTypes (propriétés).', // @translate
                    'empty_option' => '',
                    'term_as_value' =>  true,
                ],
                'attributes' => [
                    'id' => 'search-results-filters-search_property_date_range',
                    'multiple' => false,
                    'disabled' => true,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select a property…', // @translate
                ],
            ])
            */
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][search_properties_unique]',
                'type' => PropertySelect::class,
                'options' => [
                    'label' => 'Propriétés à filtrer (via Reference)', // @translate
                    'info' => 'Fonctionne seulement avec le module Référence.', // @translate
                    'empty_option' => '',
                    'term_as_value' =>  true,
                ],
                'attributes' => [
                    'id' => 'search-results-filters-search_properties_unique',
                    'multiple' => true,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select one or more properties…', // @translate
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][search_properties_custom_vocab]',
                'type' => Element\Textarea::class,
                'options' => [
                    'label' => 'Propriétés à filtrer (via CustomVocab)', // @translate
                    'info' => 'Fonctionne seulement avec le module CustomVocab. Indiquer "term | id", un par ligne.', // @translate
                ],
                'attributes' => [
                    'id' => 'search-results-filters-search_properties_custom_vocab',
                    'multiple' => true,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'dcterms:type | 5
dcterms:format | 17
',
                ],
            ])

            /*
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][search_in_item_sets]',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Filtre pour les collections (TODO fix omeka core)', // @translate
                    'info' => 'Non fonctionnel : Omeka permet seulement le "ou", pas le "et".', // @translate
                ],
                'attributes' => [
                    'id' => 'search-results-filters-search_in_item_sets',
                    'disabled' => true,
                ],
            ])
            ->add([
                'name' => 'o:block[__blockIndex__][o:data][search_item_sets]',
                'type' => ItemSetSelect::class,
                'options' => [
                    'label' => 'Sélection de collections à inclure (si choisi ci-dessus)', // @translate
                    'empty_option' => '',
                ],
                'attributes' => [
                    'id' => 'search-results-filters-search_item_sets',
                    'multiple' => 'multiple',
                    'disabled' => true,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select one or more item sets…', // @translate
                ],
            ])
            */
        ;
    }
}
