<?php
/**
 * Ce fichier est à copier dans le thème.
 */

// Utiliser null pour faire une correspondance automatique avec les noms des icones.
return null;

/**
 * Sinon correspondance entre les catégories (classes ou modèles de ressources) et les icones.
 */

return [
    // 'dcterms:Image' => 'image',
    // 'base resource' => 'default',
];
