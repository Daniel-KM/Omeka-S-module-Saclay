(function ($) {
    'use strict';

    // TODO Async requests are currently disabled.
    // TODO Works only with items currently (see php).

    // Use variables hasUser, baseUrl, baseSite, baseSiteId, baseThemeAsset,
    // categoryType, categories, filterNewSearch, recentDays, recentLasts,
    // featuredItemSetId, firstQuery, apiPerPage, and limitQuery.
    // See below for cors.

    if (typeof recentDays === 'undefined') {
        return;
    }

    // Initialisation.

    // Url to prevent cors (useless for a local api).
    // var cors = 'https://cors-anywhere.herokuapp.com/';
    var cors = '';

    // Url of the omeka api (example: http://dev.omeka.org/omeka-s-sandbox/api).
    var endpoint = cors + window.location.origin + baseUrl + 'api';

    var start = moment().subtract(recentDays, 'days');
    var end = moment();

    // Display medias progressively.
    // FIXME Check if it's really working when number is different from query.
    var limitMedia = limitQuery;

    /**
     * Duration in seconds of the cache for each query.
     *
     * @todo Finalize caching mechanism (currently, reset on load of the home page).
     */
    var cacheDuration = 3600;

    // End initialisation.

    // All queries are stored in local storage, without pagination.
    // The queries are stored with the local filter (all, featured, recent).
    // TODO Run next queries in the backend async.
    // TODO If the query "all" has all results, the local filter can use it.

    // Functions.

    /**
     * Get a list of result from api, managing temporary storage.
     *
     * The query may contain a page number.
     * @todo Move the management of the next page outside of this function.
     *
     * @param string resourceType
     * @param string query
     * @param bool next
     * @param bool asynchron
     * @param callable callback
     */
    var api = function(resourceType, query, next, asynchron, callback) {
        query = query && query.length ? query : '';
        next = !!next;
        asynchron = !!asynchron;

        var baseQuery = query;
        var currentPage = 1;
        var lastPage = 1;
        var nextPage = 0;
        var totalResults = 0;
        var timestampNow = Math.floor(Date.now() / 1000);

        var queryParams = new URLSearchParams(query);
        currentPage = queryParams.get('page');
        currentPage = currentPage.length ? parseInt(currentPage) : 1;

        // This simplifies all queries and caching.
        queryParams.set('per_page', limitQuery);

        // Append the queries to get the original url of the first media, not only the thumbnails.
        // Requires module ApiInfo. Works only for items.
        queryParams.set('append', 'urls');

        var queryParamsNoPage = new URLSearchParams(queryParams.toString());
        queryParamsNoPage.delete('page');
        // queryParamsNoPage.delete('per_page');
        queryParamsNoPage.delete('offset');
        // queryParamsNoPage.delete('limit');
        var queryNoPage = queryParamsNoPage.toString();

        var queries = JSON.parse(window.sessionStorage.getItem('queries'));
        queries = queries ? queries : {};

        if (!(resourceType in queries)) {
            queries[resourceType] = {};
            window.sessionStorage.setItem('queries', JSON.stringify(queries));
        }

        // The number of results is required to get the number of pages.
        // So an initial query without page is done to get it. It's not possible
        // to use the headers, because when args "page" and "per_page" (or offset/limit)
        // are used, the last is always the first.
        // Currently done via the server for the first page.
        // Anyway, the request should be done for the first page.
        // A refresh is done if cache is out.

        if (!(queryNoPage in queries[resourceType])
            // Wharning, the cache should be large enough to be used.
            || ((queries[resourceType][queryNoPage]['timestamp'] + cacheDuration) < timestampNow)
        ) {
            queries[resourceType][queryNoPage] = {};
            var apiInfoUrl = new URL(endpoint + '/' + resourceType + '?' + queryNoPage);
            $.ajax({url: apiInfoUrl, async: false})
                .done(function(data, textStatus, jqXHR) {
                    totalResults = jqXHR.getResponseHeader('Omeka-S-Total-Results');
                    totalResults = parseInt(totalResults) ? parseInt(totalResults) : 0;
                    // It's simpler to compute than to use header "Link"'.
                    // lastPage = extractPage(jqXHR.getResponseHeader('Link'), 'last'),
                    lastPage = Math.ceil(totalResults / limitQuery);
                    lastPage = lastPage ? lastPage : 1;
                    queries[resourceType][queryNoPage]['timestamp'] = timestampNow;
                    queries[resourceType][queryNoPage]['total_results'] = totalResults;
                    queries[resourceType][queryNoPage]['current'] = 1;
                    queries[resourceType][queryNoPage]['last'] = lastPage;
                    queries[resourceType][queryNoPage]['results'] = {
                        1: data,
                    };
                });
            window.sessionStorage.setItem('queries', JSON.stringify(queries));
        }

        totalResults = queries[resourceType][queryNoPage]['total_results'];
        lastPage = queries[resourceType][queryNoPage]['last'];

        if (next) {
            nextPage = currentPage + 1;
            if (nextPage > lastPage) {
                if (callback) {
                    var baseQueryUpdated = new URLSearchParams(baseQuery);
                    baseQueryUpdated.set('page', lastPage);
                    callback(baseQueryUpdated.toString(), null);
                }
                return;
            }
            currentPage = nextPage;
        }

        var baseQueryUpdated = new URLSearchParams(baseQuery);
        baseQueryUpdated.set('page', currentPage);

        // Get the results from the storage if available.
        if (currentPage in queries[resourceType][queryNoPage]['results']) {
            if (callback) {
                var response = {
                    'timestamp': timestampNow,
                    'total_results': totalResults,
                    'current': currentPage,
                    'last': lastPage,
                    'content': queries[resourceType][queryNoPage]['results'][currentPage],
                }
                callback(baseQueryUpdated.toString(), response);
            }
            return;
        }

        queryParams.set('page', currentPage);
        queryParams.set('per_page', limitQuery);
        var quickQuery = new URLSearchParams(queryParams.toString());
        var url = new URL(endpoint + '/' + resourceType + '?' + quickQuery.toString());
        // TODO Make all queries async (at least for loading of long list of data).
        $.ajax({url: url, async: asynchron})
            .done(function(data, textStatus, jqXHR) {
                queries[resourceType][queryNoPage]['results'][currentPage] = data;
                window.sessionStorage.setItem('queries', JSON.stringify(queries));
                if (callback) {
                    var response = {
                        'timestamp': timestampNow,
                        'total_results': totalResults,
                        'current': currentPage,
                        'last': lastPage,
                        'content': data,
                    }
                    callback(baseQueryUpdated, response);
                }
            })
            .fail(function(err) {
                console.log('Unable to fetch data from the endpoint.');
                if (callback) {
                    callback(baseQueryUpdated, null);
                }
            });
    }

    /**
     * Extract the list of related page numbers from the api header "Link".
     *
     * Available pages (relations) are: first, prev, next and last.
     *
     * @see \Omeka\Controller\ApiController: links are comma-separated.
     */
    var extractPage = function(links, relation) {
        if (!links) {
            return null;
        }
        var regex = /<(.+)>; rel="(\w+)"/g;
        links = links.split(',');
        for (var i = 0; i < links.length; i++) {
            var matches;
            while ((matches= regex.exec(links[i])) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (matches.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                if (matches[2] === relation) {
                    var url = new URL(matches[1]);
                    return parseInt(url.searchParams.get('page'));
                }
            }
        }
        return null;
    }

    /**
     * Get data of a record.
     */
    var readRecord = function (resourceType, id, callback) {
        var query = 'id=' + id;
        api(resourceType, query, false);

        var queries = JSON.parse(window.sessionStorage.getItem('queries'));
        if (!queries[resourceType]
            || !queries[resourceType][query]
            || $.isEmptyObject(queries[resourceType][query])
        ) {
            return null;
        }
        return queries[resourceType][query]['content'];
    }

    var fetchImageData = function (data) {
        var result = {
            'id': data['o:id'],
            'thumbnail': null,
            'original': null,
        };

        var asset = null;
        var media = null;

        // Check if the module ApiInfo is installed.
        if (data['o-module-api-info:append']) {
            var append = data['o-module-api-info:append'];
            if (append['o:thumbnail']) {
                result['thumbnail'] = append['o:thumbnail']['o:asset_url'];
            }

            // Check only the primary media, as in standard views.
            if (!append['o:media'] || !append['o:media'].length) {
                return result;
            }

            media = append['o:media'][0]
            if (!result['thumbnail']) {
                if (media['o:thumbnail']) {
                    result['thumbnail'] = media['o:thumbnail']['o:asset_url'];
                } else if (media['o:thumbnail_urls'] && media['o:thumbnail_urls']['medium']) {
                    result['thumbnail'] = media['o:thumbnail_urls']['medium'];
                }
            }

            result['original'] = media['o:original_url'];
            return result;
        }

        // A resource may have a thumbnail.
        if (data['o:thumbnail']) {
            asset = readRecord('assets', data['o:thumbnail']['o:id']);
            if (asset) {
                result['thumbnail'] = asset['o:asset_url'];
            }
        }

        // Check only the primary media, as in standard views.
        if (!data['o:media'] || !data['o:media'][0] || !data['o:media'][0]['o:id']) {
            return result;
        }

        media = readRecord('media', data['o:media'][0]['o:id']);
        if (!media) {
            return result;
        }

        if (!result['thumbnail']) {
            // A media may have a thumbnail too.
            if (media['o:thumbnail']) {
                asset = readRecord('assets', media['o:thumbnail']['o:id']);
                if (asset) {
                    result['thumbnail'] = asset['o:asset_url'];
                }
            }
            // If there is no thumbnail, use the standard thumbnail.
            if (!result['thumbnail']
                && media['o:thumbnail_urls']
                && media['o:thumbnail_urls']['medium']
            ) {
                result['thumbnail'] = media['o:thumbnail_urls']['medium'];
            }
        }

        // Get the original media too.
        // TODO Manage media that are not files.
        if (media['o:original_url']) {
            result['original'] = media['o:original_url'];
        }

        return result;
    }

    var replaceCards = function(query, response) {
        // TODO New search: remove filters (old behaviour)? => see the new option filterNewSearch.
        // Reset the filters to display all items before adding new ones (replaced inside appendCards()).
        // $('#portfolio-filters li[data-filter="*"]').trigger('click');

        window.sessionStorage.setItem('last_query', query);
        if (response) {
            $('#total-results span').html(response['total_results']);
        }
        if (!response || $.isEmptyObject(response['content'])) {
            $('.portfolio-item').remove();
            noMoreData();
            return;
        }
        appendData(response, true);
    };

    var appendCards = function(query, response) {
        window.sessionStorage.setItem('last_query', query);
        if (response) {
            $('#total-results span').html(response['total_results']);
        }
        if (!response || $.isEmptyObject(response['content'])) {
            noMoreData();
            return;
        }
        appendData(response, false);
    }

    var appendData = function(response, replace) {
        var data = response['content'];
        var delay = -1;

        // Update links.
        if (response['total_results'] && response['total_results'] > $('.portfolio-item').length) {
            hasMoreData();
        } else {
            noMoreData();
        }

        if (replace){
            $('.portfolio-item').remove();
            setTimeout(function() {
                $('.portfolio-container').isotope('reloadItems').isotope();
            }, 500);
        }

        // Add a nested loop to display media progressively.
        var i, j, k, m;
        for (j = 0; j < data.length; j += limitMedia) {
            m = j * limitMedia;
            for (k = 0; k < limitMedia && m + k < data.length; k++) {
                i = m + k;
                var heading = data[i]['dcterms:title'] ? data[i]['dcterms:title'][0]['@value'] : '[Sans titre]';
                var categoryId, category;
                if (categoryType === 'resource_classes') {
                    categoryId = data[i]['o:resource_class'] && data[i]['o:resource_class']['o:id'] ? data[i]['o:resource_class']['o:id'] : null;
                    category = categoryId ? categories[data[i]['o:resource_class']['o:id']]['label'] : '[Sans catégorie]';
                } else if (categoryType === 'resource_templates') {
                    categoryId = data[i]['o:resource_template'] && data[i]['o:resource_template']['o:id'] ? data[i]['o:resource_template']['o:id'] : null;
                    category = categoryId ? categories[data[i]['o:resource_template']['o:id']]['label'] : '[Sans catégorie]';
                } else {
                    categoryId = null;
                    category = 'Sans catégorie';
                }
                // To compute recent/fearured resources is useless now, because
                // the query is reset everytime.
                var isFeatured = isFeaturedResource(data[i]);
                var isRecent = isRecentResource(data[i]);
                var imageData = fetchImageData(data[i]);
                var hasMedia = imageData['original'] !== null;
                var imageUrl = imageData['thumbnail'] ? imageData['thumbnail'] : baseThemeAsset + 'img/no_image.png';

                // Same than the server.
                var $element = $(
                    '<div data-wow-delay="' + delay / 3 + 's" class="col-xl-3 col-lg-4 col-md-6 col-sm-6 portfolio-item resource item' + (isFeatured ? ' filter-featured' : '') + (isRecent ? ' filter-recent' : '') + (categoryId ? ' filter-category-' + categoryId : '') + '">' +
                        '<div class="portfolio-wrap">' +
                            '<a href="' + baseSite + '/item/' + data[i]['o:id'] + '" class="resource-link">' +
                                '<img src="' + imageUrl + '" class="img-fluid" title="' + escapeHtmlAttr(heading) + '"/>' +
                            '</a>' +
                            '<div class="portfolio-info">' +
                                '<div class="row">' +
                                    '<div class="col-6 bg-blacklight">' +
                                        '<p>' + escapeHtml(category) + '</p>' +
                                    '</div>' +
                                    (hasMedia
                                    ?
                                    '<a href="' + (hasUser ? imageData['original'] : '#loginModal') + '"' + (hasUser ? '' : ' data-toggle="modal"') + ' role="button" class="col-6 image-download bg-purplepink">' +
                                        '<div class="row">' +
                                            '<div class="col-6 no-padding">' +
                                                '<img src="' + baseThemeAsset + 'img/icons/telecharger.svg" alt="" class="filter-white">' +
                                            '</div>' +
                                            '<div class="col-6 no-padding left-margin">' +
                                                '<p>Télécharger</p>' +
                                            '</div>' +
                                        '</div>' +
                                    '</a>'
                                    :
                                    '<span class="col-6 image-download bg-purplepink">' +
                                        '<div class="row">' +
                                            '<div class="col-6 no-padding">' +
                                                '<img src="' + baseThemeAsset + 'img/icons/no_image.svg" alt="" class="filter-white">' +
                                            '</div>' +
                                            '<div class="col-6 no-padding left-margin">' +
                                                '<p>No&nbsp;media</p>' +
                                            '</div>' +
                                        '</div>' +
                                    '</span'
                                    ) +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );

                $('.portfolio-container').append($element).isotope('appended', $element);
                setTimeout(function() {
                    $('.portfolio-container').isotope('reloadItems').isotope();
                }, 500)
            }
        }
    }

    var isFeaturedResource = function (data) {
        if (featuredItemSetId > 0 && data['o:item_set'] && data['o:item_set'].length > 0) {
            for (var k = 0; k < data['o:item_set'].length; k++) {
                if (data['o:item_set'][k]['o:id'] === featuredItemSetId) {
                    return true;
                }
            }
        }
        return false;
    }

    var isRecentResource = function (data) {
        if (recentLasts.includes(data['o:id'])) {
            return true;
        }
        var now = new Date();
        var created = new Date(data['o:created']['@value']);
        return Math.floor((now - created) / 1000) < recentDays * 86400;
    }

    var hasMoreData = function () {
        $('#page_next').prop('disabled', false).html('Continuer à regarder');
        $('.no-more-results').hide();
        $('.no-more-results').removeClass('hidden');
        $('#no-more-results').find('> div').slice(1).remove();
    }

    var noMoreData = function () {
        $('#page_next').prop('disabled', true).html('Pas d’autre document');
        $('#no-more-results > div:not(first-child)').not(':first').remove();
        var message = $('#no-more-results > div:first').clone().show();
        $('#no-more-results').append(message);
        $('.no-more-results').show();
    }

    // https://stackoverflow.com/questions/24816/escaping-html-strings-with-jquery#13371349
    var escapeHtml = function (text) {
        return text.replace(/[\"&<>]/g, function (c) {
            return { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' }[c];
        });
    }

    // https://stackoverflow.com/questions/24816/escaping-html-strings-with-jquery#12034334
    var escapeHtmlAttr = function (string) {
        return String(string).replace(/[&<>"'`=\/]/g, function (c) {
            var entityMap = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#39;',
                '/': '&#x2F;',
                '`': '&#x60;',
                '=': '&#x3D;',
            };
            return entityMap[c];
        });
    }

    // End Functions.

    // Events.

    // Initialise some data.
    $(window).bind('load', function () {
        // Initialize storage, but without updating display.
        api('items', firstQuery, false, true, null);

        // Probably useless.
        portfolioIsotope.isotope({
            filter: $('#portfolio-filters > li.filter-active').attr('data-filter'),
        });

        var searchParams = new URLSearchParams(firstQuery);
        window.sessionStorage.setItem('last_query', searchParams.toString());

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cbDateRangePicker);
        cbDateRangePicker(start, end);
    });

    $('.category.indirect').click(function() {
        var categoryId = $(this).attr('data-category-id');
        if (categoryId) {
            $('#category').val(categoryId);
            document.getElementById('dropdownMenuLink').innerHTML = categories[categoryId] ? categories[categoryId]['label'] : '[Sans titre]';
        } else {
            $('#category').val('');
            document.getElementById('dropdownMenuLink').innerHTML = 'Tout';
        }
    });

    $('.category.direct').click(function() {
        var categoryId = $(this).attr('data-category-id');
        if (!categories || !categoryId) {
            return;
        }

        if (!filterNewSearch) {
            $('#portfolio-filters li[data-filter="*"]').trigger('click');
        }

        var query = (categoryType === 'resource_class' ? 'resource_class_id=' : 'resource_template_id=')
            + categoryId
            + '&site_id=' + baseSiteId
            + '&page=1'
            + '&per_page=' + limitQuery
            // Sort by created date desc by default, like in core.
            + '&sort_by=created&sort_order=desc';
        api('items', query, false, false, replaceCards);
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#categories").offset().top
        }, 500);
    });

    var portfolioIsotope = $('.portfolio-container').isotope({
        itemSelector: '.portfolio-item',
    });

    $('#portfolio-filters > li').on( 'click', function() {
        if ($(this).hasClass('filter-active')) {
            return;
        }

        $("#portfolio-filters > li").removeClass('filter-active');
        $(this).addClass('filter-active');
        portfolioIsotope.isotope({
            filter: $(this).attr('data-filter'),
        });

        // Update the query too, in order to allow to get new items, or to
        // disable the button Next.
        var query = window.sessionStorage.getItem('last_query');
        var searchParams = new URLSearchParams(query);
        searchParams.set('page', 1);

        var filter = $('#portfolio-filters > li.filter-active').attr('data-filter');

        // Remove the featured item set if featured.
        if (featuredItemSetId) {
            var itemSetId = searchParams.get('item_set_id');
            if (itemSetId === null) {
                itemSetId = searchParams.get('item_set_id[0]');
            }
            if (itemSetId && itemSetId == featuredItemSetId) {
               searchParams.delete('item_set_id');
               searchParams.delete('item_set_id[0]');
           }
        }
        // Remove the recent days if recent.
        if (recentDays) {
            searchParams.delete('datetime[0][joiner]');
            searchParams.delete('datetime[0][field]');
            searchParams.delete('datetime[0][type]');
            searchParams.delete('datetime[0][value]');
        }

        if (filter === '.filter-featured') {
            if (featuredItemSetId) {
                searchParams.set('item_set_id', featuredItemSetId);
            }
        } else if (filter === '.filter-recent') {
            if (recentDays) {
                var d = new Date();
                d.setDate(d.getDate() - recentDays);
                searchParams.set('datetime[0][joiner]', 'and');
                searchParams.set('datetime[0][field]', 'created');
                searchParams.set('datetime[0][type]', 'gt');
                // Don't use seconds to simplify storage.
                searchParams.set('datetime[0][value]', d.toISOString().slice(0, 10));
            }
        }

        $('#page_next').prop('disabled', false).html('Continuer à regarder');
        $('.no-more-results').hide();
        $('.no-more-results').removeClass('hidden');
        $('#no-more-results').find('> div').slice(1).remove();

        window.sessionStorage.setItem('last_query', searchParams.toString());
        api('items', searchParams.toString(), false, false, replaceCards);
    });

    $('#page_next').on('click', function() {
        var query = window.sessionStorage.getItem('last_query');
        setTimeout(
            function() {
                api('items', query, true, false, appendCards);
            },
            100
        );
    });

    $(window).scroll(function() {
        if ($('#page_next').prop('disabled')) {
            return;
        }
        if ($(window).scrollTop() + 1 >= $(document).height() - $(window).height()) {
            $('#page_next').trigger('click');
        }
    });

    // Start datepicker.
    function cbDateRangePicker(start, end) {
        $('#reportrange span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
    }
    // End datepicker.

    // Open advanced search dialog.
    $('#start').click(function(event) {
        if (document.getElementById('showdata')) {
            event.preventDefault();
            if (document.getElementById('showdata').style.display == 'block') {
                document.getElementById('showdata').style.display = 'none';
            } else {
                document.getElementById('showdata').style.display = 'block';
            }
        }
    })

    $('#reset').click(function(event) {
        event.preventDefault();
        var start = moment().subtract(recentDays, 'days');
        var end = moment();
        cbDateRangePicker(start, end);
        $('#showdata .list-values input[type=radio], #showdata .list-item-sets input[type=checkbox]').prop('checked', false);
        $('#showdata .list-values input[type=radio]').filter(function() { return !this.value; }).prop('checked', true);
    })

    $('#close').click(function(event) {
        event.preventDefault()
        document.getElementById('showdata').style.display = 'none';
    })
    // End advanced search dialog.

    // End events.

})(jQuery);
