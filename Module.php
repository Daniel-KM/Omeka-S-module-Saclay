<?php declare(strict_types=1);

namespace Saclay;

if (!class_exists(\Generic\AbstractModule::class)) {
    require file_exists(dirname(__DIR__) . '/Generic/AbstractModule.php')
        ? dirname(__DIR__) . '/Generic/AbstractModule.php'
        : __DIR__ . '/src/Generic/AbstractModule.php';
}

use Generic\AbstractModule;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\Session\Container;

class Module extends AbstractModule
{
    const NAMESPACE = __NAMESPACE__;

    protected $dependencies = [
        'AdvancedSearchPlus',
        'BlockPlus',
    ];

    public function attachListeners(SharedEventManagerInterface $sharedEventManager): void
    {
        $sharedEventManager->attach(
            'Omeka\Controller\Site\Item',
            'view.advanced_search',
            [$this, 'filterViewAdvancedSearch']
        );

        $sharedEventManager->attach(
            '*',
            'user.login',
            [$this, 'handleUserLogin']
        );
    }

    public function filterViewAdvancedSearch(Event $event): void
    {
        $partials = $event->getParam('partials');
        $partials[] = 'common/advanced-search/resource-template';
        $event->setParam('partials', $partials);
    }

    public function handleUserLogin(Event $event): void
    {
        // Bypass settings.
        $plugins = $this->getServiceLocator()->get('ControllerPluginManager');
        $params = $plugins->get('params');
        $redirectUrl = $params->fromQuery('redirect');
        if (!$redirectUrl) {
            $url = $plugins->get('url');
            $settings = $plugins->get('settings');
            $redirect = $settings()->get('saclay_redirect');
            switch ($redirect) {
                case empty($redirect):
                case 'home':
                    $defaultRoles = [
                        \Omeka\Permissions\Acl::ROLE_RESEARCHER,
                        \Omeka\Permissions\Acl::ROLE_AUTHOR,
                        \Omeka\Permissions\Acl::ROLE_REVIEWER,
                        \Omeka\Permissions\Acl::ROLE_EDITOR,
                        \Omeka\Permissions\Acl::ROLE_SITE_ADMIN,
                        \Omeka\Permissions\Acl::ROLE_GLOBAL_ADMIN,
                    ];
                    $user = $event->getTarget();
                    $redirectUrl = $url(in_array($user->getRole(), $defaultRoles) ? 'admin' : 'site', [], true);
                    break;
                case 'site':
                    $redirectUrl = $url('site', [], true);
                    break;
                case 'me':
                    $redirectUrl = $url('site/guest', ['action' => 'me'], [], true);
                    break;
                default:
                    $redirectUrl = $redirect;
                    break;
            }
        }

        $session = Container::getDefaultManager()->getStorage();
        $session->offsetSet('redirect_url', $redirectUrl);
    }
}
