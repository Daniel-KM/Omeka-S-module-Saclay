<?php declare(strict_types=1);

namespace Saclay;

return [
    'view_manager' => [
        'template_path_stack' => [
            dirname(__DIR__) . '/view',
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'categories' => View\Helper\Categories::class,
            'isFeatured' => View\Helper\IsFeatured::class,
            'isRecent' => View\Helper\IsRecent::class,
            'listSiteItemSets' => View\Helper\ListSiteItemSets::class,
        ],
    ],
    'block_layouts' => [
        'invokables' => [
            'searchResultsFilters' => Site\BlockLayout\SearchResultsFilters::class,
        ],
    ],
    'form_elements' => [
        'invokables' => [
            Form\ConfigForm::class => Form\ConfigForm::class,
            Form\SearchResultsFiltersFieldset::class => Form\SearchResultsFiltersFieldset::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'saclay' => [
        'config' => [
            'saclay_redirect' => 'site',
        ],
        'block_settings' => [
            'searchResultsFilters' => [
                'heading' => '',
                'resource_type' => 'items',
                'query' => [],
                'limit' => 12,
                'pagination' => true,
                'sort_headings' => [],
                'resource_template' => null,
                'template' => '',
                'filters' => [
                    'featured',
                    'recent',
                    'all',
                ],
                'filter_new_search' => false,
                'featured_item_set' => null,
                'recent_days' => 60,
                'recent_last' => 36,
                'category_type' => 'resource_classes',
                'category_resource_classes' => [],
                'category_resource_templates' => [],
                'search_property_date_range' => null,
                'search_properties_unique' => null,
                'search_properties_custom_vocab' => '',
                'search_in_item_sets' => false,
                'search_item_sets' => [],
            ],
        ],
    ],
];
